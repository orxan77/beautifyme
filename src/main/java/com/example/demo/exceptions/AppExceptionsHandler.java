package com.example.demo.exceptions;

import java.util.Date;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.example.demo.ui.model.response.ErrorMessage;

@ControllerAdvice
public class AppExceptionsHandler {

	
	// This method is responsible for handling only one exception (UserServiceException).
	// It is for handling the exceptions as we want, different than the default one.
	@ExceptionHandler(value = {UserServiceException.class})
	public ResponseEntity<Object> handleUserServiceException(UserServiceException userServiceException, WebRequest webRequest) {
		
		ErrorMessage errorMessage = new ErrorMessage(new Date(), userServiceException.getMessage());
		
		return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
		
	}
	
	// For handling all other exceptions.
	@ExceptionHandler(value = {Exception.class})
	public ResponseEntity<Object> handleUserServiceException(Exception exception, WebRequest webRequest) {
		
		ErrorMessage errorMessage = new ErrorMessage(new Date(), exception.getMessage());
		
		return new ResponseEntity<>(errorMessage, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
		
	}
	
	
	
}
