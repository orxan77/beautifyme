package com.example.demo.security;

import com.example.demo.SpringApplicationContext;

public class SecurityConstants {
	public static final long EXPIRATION_TIME = 864000000; // 10 days
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String HEADER_STRING = "Authorization";
	public static final String SIGN_UP_URL = "/users";
//	public static final String TOKEN_SECRET = "asdfgh";

	// We declared method as static, therefore we will not need to create instance
	// of SecurityConstants
	
	public static String getTokenSecret() {
		// To access Components that are created by Spring framework, we use
		// SpringApplicationContext.
		AppProperties appProperties = (AppProperties) SpringApplicationContext.getBean("appProperties");
		return appProperties.getTokenSecret();
	}
}
