package com.example.demo.io.repositeries;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.io.entity.UserEntity;

// CrudRepository interface helps us with some operations like saveUser, deleteUser, in general, CRUD operations.
// It will generate CRUD operations for the class that is in the first input of CrudRepository.
// The second input is the ID of that object.

@Repository
public interface UserRepository extends PagingAndSortingRepository<UserEntity, Long> {

	UserEntity findByEmail(String email);

	// Query method. Spring generates SQL query according to the name of method.
	// find | by | UserId = All are different keywords.
	// Run {describe users;} query in MySQL for more info.
	
	UserEntity findByUserId(String userId);
	

}
